library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.matrices.all;
use work.common.all;

-- So far untested.
-- Start with enable and hold high until the complete signal goes high.
entity Adder is
  generic (
  	matrix_a : Matrix;
  	matrix_b : Matrix;
  	matrix_c : Matrix
  );
  port (
  	clock  : in  Std_Logic;
  	enable : in  Std_Logic;
  	
  	matrix_a_accessor_port_a_request  : out Matrix_Access_Request ( index ( 
                                             row    ((addressing_size(matrix_a.n_rows)    - 1) downto 0),
                                             column ((addressing_size(matrix_a.n_columns) - 1) downto 0)));
  	matrix_a_accessor_port_a_data_out : in Unsigned((as_natural(matrix_a.bitwidth) - 1) downto 0);
  	matrix_a_accessor_port_b_request  : out Matrix_Access_Request ( index ( 
                                             row    ((addressing_size(matrix_a.n_rows)    - 1) downto 0),
                                             column ((addressing_size(matrix_a.n_columns) - 1) downto 0)));
  	matrix_a_accessor_port_b_data_out : in Unsigned((as_natural(matrix_a.bitwidth) - 1) downto 0);
  	
  	matrix_b_accessor_port_a_request  : out Matrix_Access_Request ( index ( 
                                             row    ((addressing_size(matrix_b.n_rows)    - 1) downto 0),
                                             column ((addressing_size(matrix_b.n_columns) - 1) downto 0)));
    matrix_b_accessor_port_a_data_out : in Unsigned((as_natural(matrix_b.bitwidth) - 1) downto 0);
    matrix_b_accessor_port_b_request  : out Matrix_Access_Request ( index ( 
                                             row    ((addressing_size(matrix_b.n_rows)    - 1) downto 0),
                                             column ((addressing_size(matrix_b.n_columns) - 1) downto 0)));
    matrix_b_accessor_port_b_data_out : in Unsigned((as_natural(matrix_b.bitwidth) - 1) downto 0);
  	
  	matrix_c_accessor_port_a_request : out Matrix_Access_Request ( index ( 
                                             row    ((addressing_size(matrix_c.n_rows)    - 1) downto 0),
                                             column ((addressing_size(matrix_c.n_columns) - 1) downto 0)));
  	matrix_c_accessor_port_a_data_in : out Unsigned((as_natural(matrix_c.bitwidth) - 1) downto 0);
  	matrix_c_accessor_port_b_request : out Matrix_Access_Request ( index ( 
                                             row    ((addressing_size(matrix_c.n_rows)    - 1) downto 0),
                                             column ((addressing_size(matrix_c.n_columns) - 1) downto 0)));
    matrix_c_accessor_port_b_data_in : out Unsigned((as_natural(matrix_c.bitwidth) - 1) downto 0);
  	
  	is_complete : out Std_Logic
  );
begin
  Assert are_matrices_addable(matrix_a, matrix_b)
  Report "Matrices provided to Adder are of incompatible size!"
  Severity Failure;
end entity Adder;

architecture Linear of Adder is
  constant input_bitwidth  : Natural := as_natural(matrix_a.bitwidth);
  constant output_bitwidth : Natural := as_natural(matrix_c.bitwidth);
  
  constant input_row_length     : Natural := addressing_size(matrix_a.n_rows);
  constant input_column_length  : Natural := addressing_size(matrix_a.n_columns);
  constant output_row_length    : Natural := addressing_size(matrix_c.n_rows);
  constant output_column_length : Natural := addressing_size(matrix_c.n_columns);

  function b_row_default return Unsigned is
  begin
    if matrix_a.n_columns = 1 then
      return to_unsigned(1, input_row_length);
    else
      return to_unsigned(0, input_row_length);
    end if;
  end function b_row_default;
  
  function b_column_default return Unsigned is
  begin
    if matrix_a.n_columns = 1 then
      return to_unsigned(0, input_row_length);
    else
      return to_unsigned(1, input_column_length);
    end if;
  end function b_column_default;

  signal working     : Std_Logic := '1';
  signal complete    : Std_Logic := '1';
  signal first_cycle : Std_Logic := '1';
  
  signal input_operation  : Matrix_Access_Operations(1 downto 0) := (others => No_Op);
  signal input_indices    : Matrix_Indices(1 downto 0) (
      row    ((input_row_length   ) downto 0), 
      column ((input_column_length) downto 0) 
    ) := (
      ( row    => to_unsigned(0, (input_row_length + 1)),
        column => to_unsigned(0, (input_column_length + 1)) ),
      ( row    => resize(b_row_default,    (input_row_length + 1)),
        column => resize(b_column_default, (input_row_length + 1))));
        
  signal output_operation  : Matrix_Access_Operations(1 downto 0) := (others => No_Op);
  signal output_indices    : Matrix_Indices(1 downto 0) (
      row    ((output_row_length    - 1) downto 0), 
      column ((output_column_length - 1) downto 0) 
    ) := (
      ( row    => to_unsigned(0, output_row_length),
        column => to_unsigned(0, output_column_length) ),
      ( row    => b_row_default,
        column => b_column_default));

  signal read_dispatched           : Std_Logic_Vector(1 downto 0) := B"00";
  signal write_dispatched          : Std_Logic_Vector(1 downto 0) := B"00";
  
  type Sum_Array is array (Integer range <>) of Unsigned((output_bitwidth - 1) downto 0);
  signal element_sum : Sum_Array(1 downto 0) := (
    to_unsigned(0, (output_bitwidth - 1)), 
    to_unsigned(0, (output_bitwidth - 1))
  ); 
  
begin

  is_complete <= complete;

  matrix_a_accessor_port_a_request.operator <= input_operation(0);
  matrix_b_accessor_port_a_request.operator <= input_operation(0);
  matrix_a_accessor_port_b_request.operator <= input_operation(1);
  matrix_b_accessor_port_b_request.operator <= input_operation(1);
  
  matrix_a_accessor_port_a_request.index <= input_indices(0);
  matrix_b_accessor_port_a_request.index <= input_indices(0);
  matrix_a_accessor_port_b_request.index <= input_indices(1);
  matrix_b_accessor_port_b_request.index <= input_indices(1);

  matrix_c_accessor_port_a_request.operator <= output_operation(0);
  matrix_c_accessor_port_b_request.operator <= output_operation(1);
  matrix_c_accessor_port_a_request.index    <= output_indices(0);
  matrix_c_accessor_port_b_request.index    <= output_indices(1);
  matrix_c_accessor_port_a_data_in          <= resize(matrix_a_accessor_port_a_data_out, output_bitwidth) + 
                                               resize(matrix_b_accessor_port_a_data_out, output_bitwidth);
  matrix_c_accessor_port_b_data_in          <= resize(matrix_a_accessor_port_b_data_out, output_bitwidth) + 
                                               resize(matrix_b_accessor_port_b_data_out, output_bitwidth);
--  matrix_c_accessor_port_a_data_in          <= element_sum(0);
--  matrix_c_accessor_port_b_data_in          <= element_sum(1);
  
  process (clock)
  begin
    if rising_edge(clock) then
      if working = '0' then        
        if enable = '1' then
          working  <= '1';
          complete <= '0';
        else
          working  <= '0';
          complete <= '1';
        end if;
        
      else 
        -- Check if we're all done.
        if (first_cycle = '0') and (read_dispatched = B"00") and (write_dispatched = B"00") then
          complete <= '1';
        else
          complete <= '0';
        end if;
        
        if complete = '1' then
          working     <= '0';
          first_cycle <= '1';
          
          input_indices <= (
            ( row    => to_unsigned(0, (input_row_length + 1)),
              column => to_unsigned(0, (input_column_length + 1)) ),
            ( row    => resize(b_row_default,    (input_row_length + 1)),
              column => resize(b_column_default, (input_row_length + 1))));
          output_indices <= (
            ( row    => to_unsigned(0, output_row_length),
              column => to_unsigned(0, output_column_length) ),
            ( row    => b_row_default,
              column => b_column_default));
        else
--          element_sum(0) <= resize(matrix_a_accessor_port_a_data_out, output_bitwidth) + 
--                            resize(matrix_b_accessor_port_a_data_out, output_bitwidth);
--          element_sum(1) <= resize(matrix_a_accessor_port_b_data_out, output_bitwidth) + 
--                            resize(matrix_b_accessor_port_b_data_out, output_bitwidth);
        
          working     <= '1';
          first_cycle <= '0';
           
          -- Commit any of our scheduled reads and iterate the indices.
          for i in read_dispatched'range loop
            if is_valid_index(matrix_a, input_indices(i)) then
              read_dispatched(i) <= '1';
              input_operation(i) <= Element_Read;
            else
              read_dispatched(i) <= '0';
              input_operation(i) <= No_Op;
            end if;
          end loop;
    
          for i in write_dispatched'range loop
            -- Schedule Writes and Iterate Read Indices
            if (input_operation(i) = Element_Read) then    
              write_dispatched(i)      <= '1';
              output_operation(i)      <= Element_Read_Write;
    
              input_indices(i).row    <= (input_indices(i).row + 1) 
                                           when (((input_indices(i).column + 2) / matrix_a.n_columns) > 0) 
                                           else 
                                         input_indices(i).row;
              input_indices(i).column <= (2 - (matrix_a.n_columns - input_indices(i).column)) 
                                           when (((input_indices(i).column + 2) / matrix_a.n_columns) > 0) 
                                           else 
                                         (input_indices(i).column + 2); 
            else
              write_dispatched(i) <= '0';
              output_operation(i) <= No_Op;
              input_indices(i)    <= input_indices(i);
            end if;        
    
            -- Iterate Write Indices
            if (output_operation(i) = Element_Read_Write) then
              output_indices(i).row    <= (output_indices(i).row + 1) 
                                            when (((output_indices(i).column + 2) / matrix_c.n_columns) > 0) 
                                            else 
                                          output_indices(i).row; 
              output_indices(i).column <= (2 - (matrix_c.n_columns - output_indices(i).column)) 
                                            when (((output_indices(i).column + 2) / matrix_c.n_columns) > 0) 
                                            else 
                                          (output_indices(i).column + 2); 
            else
              output_indices(i) <= output_indices(i);
            end if;
          end loop;  
        end if;   
      end if;
    end if;
  end process;
end architecture Linear;