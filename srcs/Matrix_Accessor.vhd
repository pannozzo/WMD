library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

library unisim;
use unisim.vcomponents.all;

use work.common.all;
use work.hw_info.all;
use work.matrices.all;

-- Generally tested (not comprehensively).
entity Matrix_Accessor is
  generic (
    description : Matrix;
    block_count : Natural;
    read_only   : Boolean := False
  );

  port (
    clock           : in Std_Logic;

    port_a_request  : in  Matrix_Access_Request ( index ( 
                            row    ((addressing_size(description.n_rows)    - 1) downto 0),
                            column ((addressing_size(description.n_columns) - 1) downto 0)));
    port_a_data_in  : in  Unsigned((as_natural(description.bitwidth) - 1) downto 0);
    port_a_data_out : out Unsigned((as_natural(description.bitwidth) - 1) downto 0);

    port_b_request  : in  Matrix_Access_Request ( index (
                            row    ((addressing_size(description.n_rows)    - 1) downto 0),
                            column ((addressing_size(description.n_columns) - 1) downto 0)));
    port_b_data_in  : in  Unsigned((as_natural(description.bitwidth) - 1) downto 0);
    port_b_data_out : out Unsigned((as_natural(description.bitwidth) - 1) downto 0)
  );
begin
  Assert ((block_count * block_ram_segment_size) / as_natural(description.bitwidth)) >=
          (description.n_rows * description.n_columns)
  Report "Insufficient block RAM allocated for vector's given size."
  Severity Failure;
end entity Matrix_Accessor;

-- Each of the access ports have unique ownership of their respective RAM block
-- ports. This allows them to operate completely independently.
architecture Dataflow of Matrix_Accessor is
  constant bitwidth        : Natural := as_natural(description.bitwidth);
  constant addressing_base : Natural := 3 + (bitwidth / 16);

  type Block_RAM_Interface is record
    port_a_read_enable  : Std_Logic;
    port_b_read_enable  : Std_Logic;
    port_a_write_enable : Std_Logic;
    port_b_write_enable : Std_Logic;

    port_a_data_out     : Std_Logic_Vector(15 downto 0);    
    port_b_data_out     : Std_Logic_Vector(15 downto 0);
  end record Block_RAM_Interface;

  type Block_Interface_Array is array ((block_count - 1) downto 0) of Block_RAM_Interface;
  signal block_interfaces : Block_Interface_Array;
  
  type Shared_RAM_Interface is record
    port_a_addr         : Std_Logic_Vector(13 downto 0);
    port_b_addr         : Std_Logic_Vector(13 downto 0);
    port_a_data_in      : Std_Logic_Vector(15 downto 0);
    port_b_data_in      : Std_Logic_Vector(15 downto 0);
  end record Shared_RAM_Interface;
  signal shared_interface : Shared_Ram_Interface := (
    B"11111111111111"   , B"11111111111111",
    B"0000000000000000", B"0000000000000000"  
  );

  constant flat_index_length : Natural := addressing_size(description.n_rows * description.n_columns);
  signal port_a_flat_index   : Unsigned((flat_index_length - 1) downto 0);
  signal port_b_flat_index   : Unsigned((flat_index_length - 1) downto 0);

  signal port_a_block        : Unsigned((addressing_size(block_count) - 1) downto 0);
  signal port_a_block_index  : Unsigned((13 - addressing_base) downto 0);
  signal port_b_block        : Unsigned((addressing_size(block_count) - 1) downto 0);
  signal port_b_block_index  : Unsigned((13 - addressing_base) downto 0);
begin

  ram_blocks : for i in 0 to (block_count - 1) generate
    ram_block : RAMB18E1
      generic map (
        read_width_a  => bitwidth + (bitwidth / 8),
        write_width_a => bitwidth + (bitwidth / 8),
        read_width_b  => bitwidth + (bitwidth / 8),
        write_width_b => bitwidth + (bitwidth / 8)
      )
      port map (
        -- Generic
        ClkARdClk => clock,
        ClkBWrClk => clock,

        -- Port A
        EnARdEn           => block_interfaces(i).port_a_read_enable,
        WEA(1)            => block_interfaces(i).port_a_write_enable,
        WEA(0)            => block_interfaces(i).port_a_write_enable,
        AddrARdAddr       => shared_interface.port_a_addr,
        DIADI             => shared_interface.port_a_data_in,
        DOADO             => block_interfaces(i).port_a_data_out,

        -- Port B
        EnBWrEn           => block_interfaces(i).port_b_read_enable,
        WEBWE(3 downto 2) => (others => '0'),
        WEBWE(1)          => block_interfaces(i).port_b_write_enable,
        WEBWE(0)          => block_interfaces(i).port_b_write_enable,
        AddrBWrAddr       => shared_interface.port_b_addr,
        DIBDI             => shared_interface.port_b_data_in,
        DOBDO             => block_interfaces(i).port_b_data_out,

        -- Don't Care
        DIPADIP => (others => '0'),
        DIPBDIP => (others => '0'),
        
        RegCEARegCE   => '0',
        RegCEB        => '0',
        RstRamARstRam => '0',
        RstRamB       => '0',
        RstRegARstReg => '0',
        RstRegB       => '0'
      );
      
      block_interfaces(i).port_a_read_enable  <= '1' when ((to_integer(port_a_block) = i) and 
                                                          ((port_a_request.operator = Element_Read) or
                                                           (port_a_request.operator = Element_Read_Write))) else 
                                                 '0';
      block_interfaces(i).port_a_write_enable <= '1' when ((to_integer(port_a_block) = i) and
                                                           ((port_a_request.operator = Element_Write) or
                                                            (port_a_request.operator = Element_Read_Write))) else 
                                                 '0';
      block_interfaces(i).port_b_read_enable  <= '1' when ((to_integer(port_b_block) = i) and 
                                                            ((port_b_request.operator = Element_Read) or
                                                             (port_b_request.operator = Element_Read_Write))) else 
                                                 '0';
      block_interfaces(i).port_b_write_enable <= '1' when ((to_integer(port_b_block) = i) and
                                                           ((port_b_request.operator = Element_Write) or
                                                            (port_b_request.operator = Element_Read_Write))) else 
                                                 '0';
  end generate;

  port_a_flat_index <= resize(port_a_request.index.row * to_unsigned(description.n_columns, flat_index_length) +
                              port_a_request.index.column, flat_index_length);
  port_b_flat_index <= resize(port_b_request.index.row * to_unsigned(description.n_columns, flat_index_length) +
                              port_b_request.index.column, flat_index_length);

  port_a_block       <= resize(port_a_flat_index  /  (block_ram_segment_size / bitwidth), port_a_block'length);
  port_a_block_index <= resize(port_a_flat_index mod (block_ram_segment_size / bitwidth), port_a_block_index'length);
  port_b_block       <= resize(port_b_flat_index  /  (block_ram_segment_size / bitwidth), port_b_block'length);
  port_b_block_index <= resize(port_b_flat_index mod (block_ram_segment_size / bitwidth), port_b_block_index'length);

  shared_interface.port_a_addr(13 downto addressing_base)  <= Std_Logic_Vector(port_a_block_index);
  shared_interface.port_a_data_in((bitwidth - 1) downto 0) <= Std_Logic_Vector(port_a_data_in);
  shared_interface.port_b_addr(13 downto addressing_base)  <= Std_Logic_Vector(port_b_block_index);
  shared_interface.port_b_data_in((bitwidth - 1) downto 0) <= Std_Logic_Vector(port_b_data_in);

  port_a_data_out <= resize(Unsigned(block_interfaces(to_integer(port_a_block)).port_a_data_out), bitwidth);
  port_b_data_out <= resize(Unsigned(block_interfaces(to_integer(port_b_block)).port_b_data_out), bitwidth);

end architecture Dataflow;