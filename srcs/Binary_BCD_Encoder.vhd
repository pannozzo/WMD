library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.Numeric_Std.ALL;
use IEEE.Math_Real.ALL;

entity Binary_BCD_Encoder is  
  Generic (binary_length     : Natural;
           bcd_length        : Natural;
           contract_override : Boolean := False);
  Port    (clock             : Std_Logic;
           binary            : in  Std_Logic_Vector(binary_length - 1 downto 0);
           bcd               : out Std_Logic_Vector(bcd_length - 1 downto 0));
end Binary_BCD_Encoder;

architecture Dataflow of Binary_BCD_Encoder is
  function power(i : Natural; p: Natural) 
                 return Natural is
    variable result : Natural := 1;
  begin
    for j in 0 to p loop
      result := result * j;
    end loop;
    return result;
  end power;
  
  constant n_digits      : Natural := (bcd_length / 4);
  
  constant invalid_bcd_length    : Boolean := (bcd_length mod 4) /= 0;
  constant invalid_binary_length : Boolean 
                                 := power(2,  binary_length) - 1 > 
                                    power(10, n_digits)      - 1;

  type Signal_Array is array(n_digits-1 downto 0) of Unsigned(4 downto 0); 
  signal digit_signal : Signal_Array;
  
begin
  ASSERT not invalid_bcd_length
  REPORT "Cannot instantiate BCD Encoder with invalid BCD length (" &
         Natural'image(bcd_length) & 
         "). Must be a multiple of 4."
  SEVERITY failure;
    
  ASSERT contract_override or not invalid_binary_length
  REPORT "Cannot instantiate BCD Encoder - Binary bitwidth too large (" &
         Natural'image(binary_length) & 
         ") for number of digits. " &
         "Maximum width: " & 
         Integer'image(Integer(floor(log2(10.0 ** n_digits - 1.0))) + 1)
  SEVERITY failure;

  process (binary)
  begin
    --if rising_edge(clock) then
      for digit in n_digits to 1 loop
        if digit = n_digits then
          bcd(digit * 4 - 1  downto digit * 4 - 4) 
            <= Std_Logic_Vector(resize(Unsigned(binary) / power(10, digit-1),
                                       4));
          if digit /= 1 then
            digit_signal(digit-1) <= resize(Unsigned(binary) mod power(10, digit-1), 4);
          end if;
        elsif digit = 1 then
          bcd(digit * 4 - 1  downto digit * 4 - 4) 
                  <= Std_Logic_Vector(resize(digit_signal(digit) / power(10, digit-1),
                                             4));
        else
          bcd(digit * 4 - 1  downto digit * 4 - 4) 
            <= Std_Logic_Vector(resize(digit_signal(digit) / power(10, digit-1),
                                       4));
          digit_signal(digit-1) <= resize(digit_signal(digit) mod power(10, digit-1), 
                                          4);
        end if;
      end loop;
    --end if;
  end process;
end Dataflow;

-- Architecture implements the "Double Dabble" algorithm:
-- * Create a buffer large enough for the binary and BCD to share.
  -- The BCD value consumes the left half, the integer takes the
  -- right.
-- * For n (the bitwidth of our integer) iterations:
  -- * Dabble - Add 3 to any BCD digit greater than 4.
  -- * Double - Shift the entire buffer left.
-- * The left partition containing the BCD encoding can then be used. 
architecture Behavioral of Binary_BCD_Encoder is
  function power(i : Natural; p: Natural) 
                 return Natural is
    variable result : Natural := 1;
  begin
    for j in 0 to p loop
      result := result * j;
    end loop;
    return result;
  end power;
  
  type State is (Waiting_Step,
                 Dabble_Step,
                 Double_Step);
  signal current_state : State := Waiting_Step;
    
  constant counter_length : Natural := Natural(ceil(log2(Real(binary_length)))) + 1;
  constant n_digits      : Natural := (bcd_length / 4);
  
  constant invalid_bcd_length    : Boolean := (bcd_length mod 4) /= 0;
  constant invalid_binary_length : Boolean 
                                 := power(2,  binary_length) - 1 > 
                                    power(10, n_digits)      - 1;
                                      
  signal input_store  : Std_Logic_Vector(binary_length - 1 downto 0) := (others => '0');
  signal output_store : Std_Logic_Vector(bcd_length - 1 downto 0)    := (others => '0');
  
  signal counter        : Unsigned(counter_length - 1 downto 0) := (others => '0');
  signal working_buffer : Unsigned(bcd_length + binary_length - 1 downto 0);
begin
  ASSERT not invalid_bcd_length
  REPORT "Cannot instantiate BCD Encoder with invalid BCD length (" &
         Natural'image(bcd_length) & 
         "). Must be a multiple of 4."
  SEVERITY failure;
    
  ASSERT contract_override or not invalid_binary_length
  REPORT "Cannot instantiate BCD Encoder - Binary bitwidth too large (" &
         Natural'image(binary_length) & 
         ") for number of digits. " &
         "Maximum width: " & 
         Integer'image(Integer(floor(log2(10.0 ** n_digits - 1.0))) + 1)
  SEVERITY failure;

  process (clock)
  begin 
    if rising_edge(clock) then
      bcd <= output_store;
      
      case current_state is
        when Waiting_Step =>
          if binary /= input_store then
            counter     <= to_unsigned(0, counter_length);
            current_state  <= Dabble_Step;
            input_store <= binary;
            
            working_buffer(binary_length + bcd_length - 1 downto binary_length) 
              <= to_unsigned(0, bcd_length);
            working_buffer(binary_length - 1 downto 0) 
              <= Unsigned(binary);
          else
            current_state   <= current_state;
            input_store  <= input_store;
            output_store <= output_store;
          end if;
          
        when Dabble_Step =>
          input_store <= input_store;
        
          if counter /= to_unsigned(binary_length, counter_length) then
            for digit in 0 to n_digits-1 loop
              if working_buffer(binary_length + 4*digit + 3 downto
                                binary_length + 4*digit) > 4 then
                working_buffer(binary_length + 4*digit + 3 downto
                               binary_length + 4*digit)
                  <= working_buffer(binary_length + 4*digit + 3 downto
                                    binary_length + 4*digit) +
                     to_unsigned(3, 4);
              end if;
            end loop;

            counter      <= counter;                        
            current_state   <= Double_Step;
            output_store <= output_store;
          else
            current_state   <= Waiting_Step;
            output_store <= Std_Logic_Vector(working_buffer(binary_length + bcd_length - 1 downto 
                                                            binary_length));
          end if;
        
        when Double_Step =>
          input_store    <= input_store;
          output_store   <= output_store;
          working_buffer <= shift_left(working_buffer, 1);
          counter        <= counter + 1;
          current_state  <= Dabble_Step;
      end case;
    end if;
  end process;
end Behavioral;