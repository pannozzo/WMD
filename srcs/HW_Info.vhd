library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

package hw_info is
  constant master_clock_freq : Natural := 100_000_000;
  constant sseg_display_freq : Natural := 1_000;

  constant block_ram_segment_count : Natural := 100;
  constant block_ram_segment_size  : Natural := 16_384;
end package hw_info;