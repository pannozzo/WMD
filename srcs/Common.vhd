library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

package common is
  function addressing_size(index : Natural) return Natural;
end package common;

package body common is
  function addressing_size(index : Natural) return Natural is
  begin
    return Integer(ceil(log2(Real(index))));
  end function addressing_size;
end package body common;