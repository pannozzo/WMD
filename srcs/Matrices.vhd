library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

package matrices is
  --subtype Positive is Natural range 1 to Natural'high;

  type Element_Size is (
    Width_8,
    Width_16
  );

  -- Convert the enumerated element_size type to it's corresponding
  -- natural bitwidth, allowing it to be used for ranges.
  function as_natural(s : in Element_Size) return Natural;

  type Matrix is record
    n_rows    : Positive;
    n_columns : Positive;
    bitwidth  : Element_Size;
  end record Matrix;

  function are_matrices_addable(a, b : in Matrix) return Boolean;
  function are_matrices_multipliable(a, b : in Matrix) return Boolean;

  type Matrix_Access_Operation is (
    No_Op,
    Element_Read,
    Element_Write,
    Element_Read_Write
  );
  type Matrix_Access_Operations is array (Integer range <>) of Matrix_Access_Operation;

  type Matrix_Index is record
    row    : Unsigned;
    column : Unsigned;
  end record Matrix_Index;
  type Matrix_Indices is array (Integer range <>) of Matrix_Index;

  function is_valid_index(m : in Matrix;
                          i : in Matrix_Index) return Boolean;

  function is_last_index(m : in Matrix;
                         i : in Matrix_Index) return Boolean;

  type Matrix_Access_Request is record
    operator : Matrix_Access_Operation;
    index    : Matrix_Index;
  end record Matrix_Access_Request;
end package matrices;

package body matrices is
  function as_natural(s : in Element_Size) return Natural is
  begin
    case s is
      when Width_8  => return 8;
      when Width_16 => return 16;
    end case;
end function as_natural;

  function are_matrices_addable(a, b : in Matrix) return Boolean is
  begin
  	if ((a.n_rows = b.n_rows) and (a.n_columns = b.n_columns)) then
  	  return True;
  	else
  	  return False;
  	end if;
  end function are_matrices_addable;
  
  function are_matrices_multipliable(a, b : in Matrix) return Boolean is
  begin
  	if (a.n_columns = b.n_rows) then
  	  return True;
  	else
  	  return False;
  	end if;
  end function are_matrices_multipliable;
  
  function is_valid_index(m : in Matrix;
                          i : in Matrix_Index) return Boolean is
  begin
    if ((i.row < m.n_rows) and (i.column < m.n_columns)) then
      return true;
    else
      return false;
    end if;
  end function is_valid_index;

  function is_last_index(m : in Matrix;
                   i : in Matrix_Index) return Boolean is
  begin
    if ((i.row = (m.n_rows - 1)) and (i.column = (m.n_columns - 1))) then
      return true;
    else
      return false;
    end if;
  end function is_last_index;
end package body matrices;